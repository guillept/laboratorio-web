import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AlumnosComponent } from './componentes/alumnos/alumnos.component';

import { AlumnosService } from './servicios/alumnos.service';
import { HttpClientModule } from '@angular/common/http';
import { AltaalumnosComponent } from './componentes/altaalumnos/altaalumnos.component';
import { MenuComponent } from './componentes/menu/menu.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_ROUTING } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    AlumnosComponent,
    AltaalumnosComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTING
  ],
  providers: [
    AlumnosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

/**
 * server at laboratorio-web\ClientRESTAngular\server.js
 */