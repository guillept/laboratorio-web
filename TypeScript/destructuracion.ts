let dios = {
    nombre: "El Senior de los cielos",
    apellido: "Tron 2",
    habilidad: "Javascript",
}

// let n = dios.nombre;
// let a = dios.apellido;
// let h = dios.habilidad;

let {nombre:n, apellido:a, habilidad:h} = dios;
console.log(n, a, h);

let pf:string[] = ["The", "Wall", "Dark"];
//let[a0,a1,a2] = pf;
let[,,a2] = pf; // solo obtienes algunos valores
console.log(a2);