let text:string = "Hola Mundo";
let numero:number = 9576;
let booleano:boolean = false;
let fecha:Date = new Date();
let flexibale:any;

flexibale = "Hola Mundo";
flexibale = 1234;
flexibale = false;
flexibale =  new Date();

let nombre = "Tron";
let apellido = "2.0";

let formaTradicional = "Nombre: " + nombre + "\nApellido: " + apellido  + "\n";
let templateLiteral = `Nombre: ${nombre}
Apellido: ${apellido}`;
let templateLiteralTabs =  `Nombre: ${nombre}
                            Apellido: ${apellido}`;

console.log(formaTradicional);
console.log(templateLiteral);
console.log(templateLiteralTabs);

function suma(numero1:number, numero2: number) {
    return numero1 + numero2;
}

let sumaDircta = `${1 + 2}`;
let sumaFuncion = `${suma(1,2)}`;

console.log(sumaDircta);
console.log(sumaFuncion);
/* let persona = {
    nombre: "Tron",
    apellido: "2.0"
} */