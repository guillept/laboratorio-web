function prueba(p1:string, p2:string = "default_p2", p3?:string) { //p3? optional
    if(p3) {
        console.log(`${p1} ${p2} ${p3}`);
    } else {
        console.log(`${p1} ${p2}`);
    }
}

prueba("p1");
prueba("p1", "p2");
prueba("p1", "p2", "p3");
