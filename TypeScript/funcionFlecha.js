"use strict";
let operacionNormal = function suma(n1, n2) {
    console.log(`En funcion normal. numero1: ${n1}, numero2: ${n2}`);
    return n1 + n2;
};
let operacionArroFunction = (n1, n2) => {
    console.log(`En Arrow Fucntion. n: ${n1}, numero2: ${n2}`);
    return n1 + n2;
};
console.log(operacionNormal(1, 2));
console.log(operacionArroFunction(1, 2));
let persona = {
    nombre: "Tron",
    apellido: "Aguirre",
    imprime() {
        setTimeout(() => {
            console.log(`${this.nombre} ${this.apellido}`);
        }, 3000);
    }
};
persona.imprime();
//# sourceMappingURL=funcionFlecha.js.map