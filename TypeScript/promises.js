"use strict";
let promise = new Promise(function (resolve, reject) {
    setTimeout(() => {
        console.log("Ejecucion asincrona finalizada...");
        resolve(); //resuelve el promise
        //reject();
    }, 3000);
});
console.log("Operacion 1");
promise.then(function () {
    console.log("Ejecucion sincrona finalizo con exito!"); //se invoca en el resovle
}, function () {
    console.log("Ejecucion sincrona finalizo con error!"); //se invoca en el reject;
});
console.log("Operacion 2");
console.log("Operacion 3");
//# sourceMappingURL=promises.js.map