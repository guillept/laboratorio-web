"use strict";
function prueba(p1, p2 = "default_p2", p3) {
    if (p3) {
        console.log(`${p1} ${p2} ${p3}`);
    }
    else {
        console.log(`${p1} ${p2}`);
    }
}
prueba("p1");
prueba("p1", "p2");
prueba("p1", "p2", "p3");
//# sourceMappingURL=funciones.js.map