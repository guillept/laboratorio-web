"use strict";
let text = "Hola Mundo";
let numero = 9576;
let booleano = false;
let fecha = new Date();
let flexibale;
flexibale = "Hola Mundo";
flexibale = 1234;
flexibale = false;
flexibale = new Date();
let nombre = "Tron";
let apellido = "2.0";
let formaTradicional = "Nombre: " + nombre + "\nApellido: " + apellido + "\n";
let templateLiteral = `Nombre: ${nombre}
Apellido: ${apellido}`;
let templateLiteralTabs = `Nombre: ${nombre}
                            Apellido: ${apellido}`;
console.log(formaTradicional);
console.log(templateLiteral);
console.log(templateLiteralTabs);
function suma(numero1, numero2) {
    return numero1 + numero2;
}
let sumaDircta = `${1 + 2}`;
let sumaFuncion = `${suma(1, 2)}`;
console.log(sumaDircta);
console.log(sumaFuncion);
/* let persona = {
    nombre: "Tron",
    apellido: "2.0"
} */ 
//# sourceMappingURL=templateliterals.js.map