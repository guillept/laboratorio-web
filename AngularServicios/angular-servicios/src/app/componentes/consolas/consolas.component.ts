import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 

@Component({
  selector: 'app-infoconsola', 
  templateUrl: './infoconsola.component.html', 
  styleUrls: ['./infoconsola.component.css']
}) 
export class InfoconsolaComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      console.log(params['id']);
    })
  }
  ngOnInit() { }
} 
