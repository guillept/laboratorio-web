import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-pipes-ng';

  mayusculas:string = "hola mundo";
  minusculas:string = "HOLA mundo minuscuulas";
  juegos:string = "Super Mario Bros 3";
  numeros = [0,1,2,3,4,5,6,7,8,9];
  decimal = 1234.56789;
  percentage = 0.12345 ;
  moneda = 789123.456;
  json =
    {
      'hola' : 'mundo',
      'hello' : 'world'
    };

    asinc = new Promise ((resolve, reject) => {
      setTimeout(() => {
        resolve('Procesando ando');
      }, 5000);
    });

    fecha = new Date();
}
