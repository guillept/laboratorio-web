import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //tag html -> <app-root></app-root>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aplicacionbasica';

  album:string = "Casi todos somos blancos";
  artista:string = "STB";
  lanzamiento:string = "2019"
}
